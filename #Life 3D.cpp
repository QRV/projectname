#include <iostream>
#include <iomanip>
#define H 30
#define W 75
#define L 75
using namespace std;

void Info()
{
    cout << "The game of Life;" <<endl <<endl;
    cout << "Rules: You enter the coordinates of the living points, then after you enter the Start the game begins.";
         cout << "If in a cage no body and around the cage there are 3 living neighbor, the cell alive."   <<endl;
              cout << "If in cell here is a living organism and a neighbor exactly 2, the cell continues to live."  <<endl;
                   cout << "In all other cases, more than 3 neighbor or less 2 cells die"  <<endl <<endl;
    cout <<setw(200)<< "Enjoy the game!!";
}

void clear(bool mat[H][W][L])
{
    for (int m = 0; m < H; m++)
    {
        for (int n = 0; n < W; n++)
        {
            for (int k = 0; k < L; k++)
            {
                mat[m][n][k] = 0;
            }
        }
    }
}

void print(bool mat[H][W][L])
{
    //cout << setw(3) << " ";
    //for (int p = 0; 5*p < W; p++) cout << setw(5) << 5*p+1;
    cout << endl;
    for (int m = 0; m < H; m++)
    {
        //cout << setw(3); //<< m+1;
        for (int n = 0; n < W; n++)
        {
            //cout <<setw(3); //<< n+1;
            for (int k = 0; k < L; k++)
            {
                if (mat[m][n][k]) cout << "\xDB";
                else cout << "-";
            }
            cout <<endl;
        }
        cout << endl;
    }
}



void calculate(bool mata[H][W][L], bool matb[H][W][L])
{
    unsigned int neighbors;
    for (int m = 1; m < H-1; m++)
    {
        for (int n = 1; n < W-1; n++)
        {
            for (int k =1; k < L-1; k++)
            {
                neighbors = 0;
                //�������� ���������
                if (mata[m-1][n-1][k-1] == 1) neighbors += 1;
                if (mata[m+0][n-1][k-1] == 1) neighbors += 1;
                if (mata[m+1][n-1][k-1] == 1) neighbors += 1;
                if (mata[m-1][n+0][k-1] == 1) neighbors += 1;
                if (mata[m+0][n+0][k-1] == 1) neighbors += 1;
                if (mata[m+1][n+0][k-1] == 1) neighbors += 1;
                if (mata[m-1][n+1][k-1] == 1) neighbors += 1;
                if (mata[m+0][n+1][k-1] == 1) neighbors += 1;
                if (mata[m+1][n+1][k-1] == 1) neighbors += 1;
                //������ ���������
                if (mata[m-1][n-1][k+1] == 1) neighbors += 1;
                if (mata[m+0][n-1][k+1] == 1) neighbors += 1;
                if (mata[m+1][n-1][k+1] == 1) neighbors += 1;
                if (mata[m-1][n+0][k+1] == 1) neighbors += 1;
                if (mata[m+0][n+0][k+1] == 1) neighbors += 1;
                if (mata[m+1][n+0][k+1] == 1) neighbors += 1;
                if (mata[m-1][n+1][k+1] == 1) neighbors += 1;
                if (mata[m+0][n+1][k+1] == 1) neighbors += 1;
                if (mata[m+1][n+1][k+1] == 1) neighbors += 1;
                //� ��������� ������ �������
                if (mata[m-1][n-1][k] == 1) neighbors += 1;
                if (mata[m+0][n-1][k] == 1) neighbors += 1;
                if (mata[m+1][n-1][k] == 1) neighbors += 1;
                if (mata[m-1][n+0][k] == 1) neighbors += 1;
                //if (mata[m+0][n+0][k] == 1) neighbors += 1;
                if (mata[m+1][n+0][k] == 1) neighbors += 1;
                if (mata[m-1][n+1][k] == 1) neighbors += 1;
                if (mata[m+0][n+1][k] == 1) neighbors += 1;
                if (mata[m+1][n+1][k] == 1) neighbors += 1;



                /*if (mata[m][n] == 1 && neighbors < 2)
                    matb[m][n] = 0;
                else if (mata[m][n] == 1 && neighbors > 3)
                    matb[m][n] = 0;
                else if (mata[m][n] == 1 && (neighbors == 2 || neighbors == 3))
                    matb[m][n] = 1;
                else if (mata[m][n] == 0 && neighbors == 3)
                    matb[m][n] = 1;*/
                if ( ( (mata[m][n][k] == 1) && (neighbors == 3 || neighbors == 2) ) || ( (mata[m][n][k] == 0) && (neighbors == 3) ) )
                    matb[m][n][k] = 1;
                else matb[m][n][k] = 0;

            }
        }
    }
}

void swap(bool mata[H][W][L], bool matb[H][W][L])
{
    for (int m = 0; m < H; m++)
    {
        for (int n = 0; n < W; n++)
        {
            for (int k = 0; k < L; k++)
            {
                mata[m][n][k] = matb[m][n][k];
            }
        }
    }
}

int main()
{
    bool now[H][W][L], next[H][W][L];
    int x, y, z, cont;
    Info();
    cout << left;
    cin.get();

    clear(now);
    print(now);

    do
    {
        cin >> x;
        if (x == -1) break;
        cin >> y;
        cin >> z;
        now[y-1][x-1][z-1] = 1;
        print(now);
    }
    while(x != -1);

    do
    {
        clear(next);
        calculate(now, next);
        swap(now, next);
        print(now);
        cin>>cont;
    }
    while(cont != -1);

    return 0;
}
